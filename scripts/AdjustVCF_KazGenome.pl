#!/usr/local/bin/perl
#this program goes through a table of locations of scaffolds on chromosomes
#And ajusts the corresponding VCF file to change coordinates on scaffolds to coordinates on chromosomes

#usage: perl AdjustVCF.pl Table_of_Locations MappedSample.vcf

my $input = $ARGV[0];
my $vcf = $ARGV[1];
open (INPUT, "$input") or die "can't find $input";
open (RESULTS, ">$vcf.adjusted");
$suml=0;


while ($line=<INPUT>)
	{
	#skip first (empty) line
	next if $. == 1;
	chomp $line;
	#get sequence length
	(@dnas)=split ("\t", $line);
	$chrom{$dnas[0]} = $dnas[1];
	$coords{$dnas[0]} = $dnas[2];
	}

print "Loaded table of locations, now adjusting VCF.\n";

open (VCF, "$vcf") or die "can't find $vcf";
while ($snp=<VCF>)
	{
	if ($snp =~ /^#CHROM/)
		{
		print RESULTS $snp;
		}
	elsif ($snp =~ /^#/)
		{
		}
	else
		{
		chomp $snp;
		(@vcfcols)=split ("\t", $snp);
		$newchrom = $chrom{$vcfcols[0]};
		$newcoord = $vcfcols[1] + $coords{$vcfcols[0]};
		shift @vcfcols;
		shift @vcfcols;
		$othervcf =  join ("\t", @vcfcols);
		if ($newchrom =~ /[0-9]/)
			{
			print RESULTS "$newchrom\t$newcoord\t$othervcf\n";
			}

		}
	}
