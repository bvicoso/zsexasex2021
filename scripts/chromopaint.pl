#!/usr/local/bin/perl
#this program goes through a vcf
#And counts for each scaffold the number of sites supporting aibi ancestry (0/1 or 1/1) versus only kaz (0/0)
#It outputs a file.counts which contains, for each sample, the number of aib and kaz SNPs (sample1_0 sample1_1 sample2_0 sample2_1 etc)
#and a file file.proportion which has the proportion of SNPs with aib ancestry for each sample/scaffold
#when fewer than 10 informative SNPs were found on a scaffold/sample, the proportion is NA

#usage: perl chromopaint.pl MappedSample.vcf

my $vcf = $ARGV[0];
open (RESULTS, ">$vcf.counts");
open (RESULTS2, ">$vcf.proportion");
open (VCF, "$vcf") or die "can't find $vcf";

$scafname="yoyoyo";
my %num1;
my %num0;

while ($snp=<VCF>)
	{
	chomp $snp;
	next if ($snp =~ /#/);

	(@vcfcols)=split ("\t", $snp);
	my $samplenumber = scalar(@vcfcols)-9;	

print "The VCF has $samplenumber samples!\n";
	
	#if we're staying on the same scaffold or starting the counts
	if ($vcfcols[0] eq $scafname || $scafname eq "yoyoyo")
		{
		$scafname = $vcfcols[0];

		shift @vcfcols for 1..9;

		$counter = 0;
		foreach $rec (@vcfcols)
			{
			$counter=$counter+1;

			if ($rec =~ /0\/1/ || $rec =~ /1\/1/)
				{
				$num1{$counter} = $num1{$counter}+1;
				$num0{$counter} = $num0{$counter}+0;

				}
			elsif ($rec =~ /0\/0/)
				{
				$num0{$counter} = $num0{$counter}+1;
				$num1{$counter} = $num1{$counter}+0;

				}
			else 
				{
				$num0{$counter} = $num0{$counter}+0;
				$num1{$counter} = $num1{$counter}+0;

				}



			}

		}

	#if we now got a new scaffold
	else
		{
		#output results and clear hashes
print "$scafname\n";
		print RESULTS "$scafname\t";
		print RESULTS2 "$scafname\t";
		@samplesarray = (1..$samplenumber);
		foreach $sample (@samplesarray) {  # foreach synonym
			print RESULTS "$num0{$sample}\t";
			print RESULTS "$num1{$sample}\t";
			if (($num0{$sample}+$num1{$sample})>10 )
				{
				$ratio = ($num1{$sample}) / ($num1{$sample} + $num0{$sample});
				print RESULTS2 "$ratio\t";
				}
			else 
				{
				print RESULTS2 "NA\t";
				}
			}
		print RESULTS "\n";
		print RESULTS2 "\n";

		#then start counting again
		%num0 = ();
		%num1 = ();

		$scafname = $vcfcols[0];
		shift @vcfcols for 1..9;
			
		$counter = 0;
		foreach $rec (@vcfcols)
			{
			$counter=$counter+1;
			if ($rec =~ /0\/1/ || $rec =~ /1\/1/)
				{
				$num1{$counter} = $num1{$counter}+1;
				$num0{$counter} = $num0{$counter}+0;

				}
			elsif ($rec =~ /0\/0/)
				{
				$num0{$counter} = $num0{$counter}+1;
				$num1{$counter} = $num1{$counter}+0;

				}
			else 
				{
				$num0{$counter} = $num0{$counter}+0;
				$num1{$counter} = $num1{$counter}+0;

				}
			}

		}




	}


#output results fpr final scaffold
print RESULTS "$scafname\t";
print RESULTS2 "$scafname\t";
foreach $sample (@samplesarray) {  # foreach synonym
			print RESULTS "$num0{$sample}\t";
			print RESULTS "$num1{$sample}\t";
			if (($num0{$sample}+$num1{$sample})>10 )
				{
				$ratio = ($num1{$sample}) / ($num1{$sample} + $num0{$sample});
				print RESULTS2 "$ratio\t";
				}
			else 
				{
				print RESULTS2 "NA\t";
				}
			}

print RESULTS "\n";
print RESULTS2 "\n";
