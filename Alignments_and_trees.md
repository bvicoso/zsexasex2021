## Alignments of the W candidates and their Z homologs:

The shared W candidates of A. sinica and A. franciscana, their Z homologs, and the outgroup sequences were aligned

Input files: 

candidates_zw.txt

Artemia_sinica_W_candidates.fasta

Artemia_franciscana_W_candidates.fasta

Artemia_franciscana_wholebody_and_tissues_primary_transcriptomes_pooled.fasta

Artemia_sinica_denovo_and_guided_primary_transcriptomes_pooled.fasta

```
module load blat
module load seqtk
module load mafft

for (( i = 1; i <= 16; i++ ))

do

sed -n "$i"p  candidates_zw.txt | cut -f 1 | sort | uniq | seqtk subseq Artemia_sinica_W_candidates.fasta /dev/stdin >> "$i".fa

sed -n "$i"p  candidates_zw.txt | cut -f 2 | sort | uniq | seqtk subseq Artemia_franciscana_W_candidates.fasta /dev/stdin >> "$i".fa

sed -n "$i"p  candidates_zw.txt | cut -f 3 | sort | uniq | seqtk subseq Artemia_sinica_denovo_and_guided_primary_transcriptomes_pooled.fasta /dev/stdin >> "$i".fa

sed -n "$i"p  candidates_zw.txt | cut -f 4 | sort | uniq | seqtk subseq Artemia_franciscana_wholebody_and_tissues_primary_transcriptomes_pooled.fasta /dev/stdin >> "$i".fa

sed -n "$i"p  candidates_zw.txt | cut -f 5 | sort | uniq | seqtk subseq Branchinecta_lindahli_clean_headers.fasta /dev/stdin >> "$i".fa

#mafft --adjustdirection "$i".fa > "$i"_2.fa
done
```
The trees were made on [Phlyogeny.fr](http://www.phylogeny.fr/advanced.cgi) and then edited on [https://itol.embl.de/](https://itol.embl.de/).

# Alignments without the outgroup:
```
module load blat
module load seqtk
module load mafft

for (( i = 1; i <= 16; i++ ))

do

sed -n "$i"p  candidates_zw.txt | cut -f 1 | sort | uniq | seqtk subseq Artemia_sinica_W_candidates.fasta /dev/stdin >> "$i"_no.fa

sed -n "$i"p  candidates_zw.txt | cut -f 2 | sort | uniq | seqtk subseq Artemia_franciscana_W_candidates.fasta /dev/stdin >> "$i"_no.fa

sed -n "$i"p  candidates_zw.txt | cut -f 3 | sort | uniq | seqtk subseq Artemia_sinica_denovo_and_guided_primary_transcriptomes_pooled.fasta /dev/stdin >> "$i"_no.fa

sed -n "$i"p  candidates_zw.txt | cut -f 4 | sort | uniq | seqtk subseq Artemia_franciscana_wholebody_and_tissues_primary_transcriptomes_pooled.fasta /dev/stdin >> "$i"_no.fa

mafft --adjustdirection "$i"_no.fa > "$i"_no_2.fa
done
```
All the alignments can be found here

