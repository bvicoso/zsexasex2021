## Important Details:
Samples 16-20 are asexuals, 21-30 are putative sexuals.

The same parameters were run for asexuals and sexuals, unless stated otherwise.

### 1. Subset reads for the pooled analysis

Let's make sure we start with the same number of reads per female in each pool (asexuals and controls).

WD: /nfs/scistore18/vicosgrp/bvicoso/Artemia_FST_Melissa/KazRef/1b-ReadSubset

Get reads:

```
ln -s ../1-Reads/*[0-9].fq[12] .
```

The smallest number of reads that we have for any female is: 40676997
So let's subset each sample to that number. 

```
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex1.fq1 40676997 > asex1_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex1.fq2 40676997 > asex1_sub.fq2
```

<details><summary>The rest of the commands are hidden here.</summary>

```
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex2.fq1 40676997 > asex2_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex2.fq2 40676997 > asex2_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex3.fq1 40676997 > asex3_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex3.fq2 40676997 > asex3_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex4.fq1 40676997 > asex4_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex4.fq2 40676997 > asex4_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex5.fq1 40676997 > asex5_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex5.fq2 40676997 > asex5_sub.fq2

##Controls

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex1.fq1 40676997 > sex1_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex1.fq2 40676997 > sex1_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex2.fq1 40676997 > sex2_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex2.fq2 40676997 > sex2_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex3.fq1 40676997 > sex3_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex3.fq2 40676997 > sex3_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex4.fq1 40676997 > sex4_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex4.fq2 40676997 > sex4_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex5.fq1 40676997 > sex5_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex5.fq2 40676997 > sex5_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex6.fq1 40676997 > sex6_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex6.fq2 40676997 > sex6_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex7.fq1 40676997 > sex7_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex7.fq2 40676997 > sex7_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex8.fq1 40676997 > sex8_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex8.fq2 40676997 > sex8_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex9.fq1 40676997 > sex9_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex9.fq2 40676997 > sex9_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex10.fq1 40676997 > sex10_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex10.fq2 40676997 > sex10_sub.fq2
```

</details>


To check that we recovered the same set of forward and reverse reads:

```
grep '@A00' asex1_sub.fq2 | perl -pi -e 's/\/[12]//gi' | md5sum
grep '@A00' asex1_sub.fq1 | perl -pi -e 's/\/[12]//gi' | md5sum
```


### 2. Trimming 
```
java -jar trimmomatic-0.39.jar PE sample.1.fq sample.2.fq sample.1.paired.fq sample.1.unpaired.fq sample.2.paired.fq sample.2.unpaired.fq ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10:2:keepBothReads LEADING:3 TRAILING:3 MINLEN:36
```

### 3. Indexing the genome
```
module load bowtie2/2.4.4 
bowtie2-build Artemia_sinica_genome_29_12_2021.fasta  Sinica.index
```

### 4. Align Reads
```
module load bowtie2/2.4.4 
bowtie2 --local --sensitive --no-unal  -p 8 -x Sinica.index -1 sample.1.paired.fq -2 sample.2.paired.fq -S sample.sam
```
### 5. Convert sam to bam
```
module load samtools/1.13
samtools view -bS sample.sam > sample.bam
```

### 6. Sort bam
```
samtools sort -@ 8 sample.bam > sample.sort.bam
```



## Pooled Analyses
### 7. Merge bamfiles into sex and asex bamfiles
```
samtools merge Sex.bam sample21.sort.bam sample22.sort.bam sample23.sort.bam sample24.sort.bam sample25.sort.bam sample26.sort.bam sample27.sort.bam sample28.sort.bam sample29.sort.bam sample30.sort.bam
samtools merge Asex.bam sample16.sort.bam sample17.sort.bam sample18.sort.bam sample19.sort.bam sample20.sort.bam
samtools view -q 20 -@ 8 -bS Asex.bam | samtools sort > Asex.sort.bam
samtools view -q 20 -@ 8 -bS Sex.bam | samtools sort > Sex.sort.bam
```
### 8. Make pileup
```
samtools mpileup -B Asex.sort.bam Sex.sort.bam > Asex_Sex.mpileup
```
### 9. Run Popoolation
```
java -ea -Xmx7g -jar /Popoolation2/popoolation2_1201/mpileup2sync.jar --input Asex_Sex.mpileup --output Asex_Sex_java.sync --fastq-type sanger --min-qual 20 --threads 2

#per SNP
perl Popoolation2/popoolation2_1201/fst-sliding.pl --input Asex_Sex_java.sync --output Asex_Sex.persnp.fst --suppress-noninformative --min-count 2 --min-coverage 10 --max-coverage 2000 --min-covered-fraction 1 --window-size 1 --step-size 1 --pool-size 5:10


#per 1kb
perl Popoolation2/popoolation2_1201/fst-sliding.pl --input Asex_Sex_java.sync --output Asex_Sex.1kb.min10.fst --suppress-noninformative --min-count 2 --min-coverage 10 --max-coverage 2000 --min-covered-fraction 1 --window-size 1000 --step-size 1000 --pool-size 5:10
```

### 10. R Script to make figures
[FST.LG.1kb.AsexSex.Popoolation.R](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/FST.LG.1kb.AsexSex.Popoolation.R)

infile2 = Asex_Sex.persnp.fst.modified.input.txt (FST per SNP)

infile1 = Asex_Sex.1kb.try3.fst.modifiedinput.txt (FST per 1kb)


