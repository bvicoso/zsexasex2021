# Infer which regions were inherited from the rare male by F2 asexual and control females


## Reads and subsetting


### Reads used 

First we copy all reads to 1-Reads folder.

SampleID | Species/population | Sex
--- | --- | ---
168701 | Artemia sp. Kazakhstan | Male
168702 | Artemia sp. Kazakhstan | Female
99732 | Artemia parthenogenetica Aibi Lake | Rare Male
99733 | Artemia parthenogenetica Aibi Lake | Female
125916 | Aibi rare male X Kazakhstan female F2 | asexual female
125917 | Aibi rare male X Kazakhstan female F2 | asexual female
125918 | Aibi rare male X Kazakhstan female F2 | asexual female
125919 | Aibi rare male X Kazakhstan female F2 | asexual female
125920 | Aibi rare male X Kazakhstan female F2 | asexual female
125921 | Aibi rare male X Kazakhstan female F2 | sexual female
125922 | Aibi rare male X Kazakhstan female F2 | sexual female
125923 | Aibi rare male X Kazakhstan female F2 | sexual female
125924 | Aibi rare male X Kazakhstan female F2 | sexual female
125925 | Aibi rare male X Kazakhstan female F2 | sexual female
125926 | Aibi rare male X Kazakhstan female F2 | sexual female
125927 | Aibi rare male X Kazakhstan female F2 | sexual female
125928 | Aibi rare male X Kazakhstan female F2 | sexual female
125929 | Aibi rare male X Kazakhstan female F2 | sexual female
125930 | Aibi rare male X Kazakhstan female F2 | sexual female



### Read subsetting

Since indiduals from the parental lines and asexuals have systematically higher coverage than F2 sexual females, let's subset the same number of reads as for the sexual with the largest read number (59356840 reads). Since the reads for the rare male and aibi asexual female are shorter, we adjust this number to obtain the same coverage in these two samples (71228208 reads). No subsetting was done for the F2 sexuals.

For A. sp. Kazahstan female:

```
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 kazF.fq1.gz 59356840 > kazF_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 kazF.fq2.gz 59356840 > kazF_sub.fq2
```

<details><summary>The others are hidden here.</summary>

```
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 kazF.fq1.gz 59356840 > kazF_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 kazF.fq2.gz 59356840 > kazF_sub.fq2
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 kazM.fq1.gz 59356840 > kazM_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 kazM.fq2.gz 59356840 > kazM_sub.fq2
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 aibF.fq1 71228208 > aibF_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 aibF.fq2 71228208 > aibF_sub.fq2
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 aibM.fq1 71228208 > aibM_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 aibM.fq2 71228208 > aibM_sub.fq2
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex1.fq1 59356840 > asex1_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex1.fq2 59356840 > asex1_sub.fq2
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex2.fq1 59356840 > asex2_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex2.fq2 59356840 > asex2_sub.fq2
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex3.fq1 59356840 > asex3_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex3.fq2 59356840 > asex3_sub.fq2
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex4.fq1 59356840 > asex4_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex4.fq2 59356840 > asex4_sub.fq2
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex5.fq1 59356840 > asex5_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex5.fq2 59356840 > asex5_sub.fq2
```

</details>


## Mapping to Kaz genome

Get the A. sp. Kazakhstan genome assembly:

```
cp /nfs/scistore03/vicosgrp/melkrewi/Blank_slate_shrimp/kazakh_new_genome_male/Assembly_mega/k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq Akaz_16092021.fa
```

Get reads:

```
ln -s ../1-Reads/*sub* .
ln -s ../1-Reads/sex* .
```

Then map with BWA:

```
module load bwa
module load samtools

bwa index Akaz_16092021.fa

bwa mem -M -t 30 Akaz_16092021.fa kazF_sub.fq1  kazF_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > kazF.bam
bwa mem -M -t 30 Akaz_16092021.fa kazM_sub.fq1  kazM_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > kazM.bam
```

<details><summary>The other mapping commands are hidden here.</summary>

```
bwa mem -M -t 30 Akaz_16092021.fa aibF_sub.fq1  aibF_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > aibF.bam
bwa mem -M -t 30 Akaz_16092021.fa aibM_sub.fq1  aibM_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > aibM.bam

bwa mem -M -t 30 Akaz_16092021.fa asex1_sub.fq1  asex1_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > asex1.bam
bwa mem -M -t 30 Akaz_16092021.fa asex2_sub.fq1  asex2_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > asex2.bam
bwa mem -M -t 30 Akaz_16092021.fa asex3_sub.fq1  asex3_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > asex3.bam
bwa mem -M -t 30 Akaz_16092021.fa asex4_sub.fq1  asex4_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > asex4.bam
bwa mem -M -t 30 Akaz_16092021.fa asex5_sub.fq1  asex5_sub.fq2 | samtools view -F 4 -b | samtools sort -T individual > asex5.bam

bwa mem -M -t 50 Akaz_16092021.fa sex1.fq1  sex1.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex1b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex2.fq1  sex2.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex2b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex3.fq1  sex3.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex3b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex4.fq1  sex4.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex4b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex5.fq1  sex5.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex5b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex6.fq1  sex6.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex6b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex7.fq1  sex7.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex7b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex8.fq1  sex8.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex8b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex9.fq1  sex9.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex9b.bam
bwa mem -M -t 50 Akaz_16092021.fa sex10.fq1  sex10.fq2 | samtools view -F 4 -b | samtools sort -T individual > sex10b.bam

```

</details>

## SNP calling

```
srun samtools faidx Akaz_16092021.fa

srun bcftools mpileup --threads 50 -a AD,DP,SP -Ou -f Akaz_16092021.fa kazF.bam kazM.bam aibF.bam aibM.bam asex1.bam asex2.bam asex3.bam asex4.bam asex5.bam sex1b.bam  sex2b.bam sex3b.bam sex4b.bam sex5b.bam sex6b.bam sex7b.bam sex8b.bam sex9b.bam sex10b.bam | bcftools call -v -f GQ,GP -mO z -o AsexSexSubset.vcf.gz --threads 50

srun vcftools --gzvcf AsexSexSubset.vcf.gz  --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 4 --max-meanDP 100 --minDP 4 --maxDP 50 --recode --stdout >  head_filtered.vcf
```

Select only SNPs alternatively fixed in Kaz and Aib:
```
cat head_filtered.vcf | awk '(($10 ~ "0/0" && $11 ~ "0/0" && $12 ~ "1/1" && $13 ~ "1/1") || ($1 ~ "#"))' > head_filtered_fixedSNP.vcf 
```

--> 600233 SNPs

Subset so we keep only F2 samples in the VCF:
```
bcftools view -s asex1.bam,asex2.bam,asex3.bam,asex4.bam,asex5.bam,sex1b.bam,sex2b.bam,sex3b.bam,sex4b.bam,sex5b.bam,sex6b.bam,sex7b.bam,sex8b.bam,sex9b.bam,sex10b.bam head_filtered_fixedSNP.vcf > head_filtered_fixedSNP_sexAsex.vcf
```

# check number of individuals that carry heterozygous alleles

Since we start the cross with diverged individuals, we expect that many sites will be heterozygous in ~50% of F2 females. Let's check if this is the case.

```
cat head_filtered_sexAsex.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | grep -v '\.\/\.' | grep -v '1\/1' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/0\/0/0/gi' | awk '{print $10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21+$22+$23+$24}' | sort | uniq -c | awk '{print $2,$1}' | sort -g
```

Gives:

```
0 323
1 5943
2 151039
3 354179
4 359348
5 316941
6 313618
7 216491
8 178252
9 148545
10 124195
11 108900
12 110892
13 124352
14 167119
15 357975
```

So the peak at around 1/2 of females is there, but there is a huge excess of heterozygous sites found in most/all females, which is likely driven by repeats.

When we focus on the sites that are alternative fixed between the parental lineages (A. sp. Kaz individuals are 0/0, Aibi lake individuals are 1/1):

```
head_filtered_fixedSNP_sexAsex.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | grep -v '\.\/\.' | grep -v '1\/1' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/0\/0/0/gi' | awk '{print $10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21+$22+$23+$24}' | sort | uniq -c | awk '{print $2,$1}' | sort -g
```

We get a much cleaner picture:
```
0 216
1 925
2 4692
3 17145
4 10399
5 25205
6 47217
7 28761
8 22955
9 13268
10 9696
11 2103
12 1535
13 1747
14 3758
15 7696
```

So let's proceed using these filtered data. 


## Fst between F2 asexual females and control "sexual" females

Make files specifying population:
```
cat ../4-BasicStats/head_filtered_fixedSNP.het | cut -f 1 | grep 'asex' > population_1.txt
cat ../4-BasicStats/head_filtered_fixedSNP.het | cut -f 1 | grep 'b.bam' > population_2.txt
```

Then 

```
module load bcftools
module load vcftools

#Run in queue
vcftools --vcf head_filtered_fixedSNP_sexAsex.vcf --weir-fst-pop population_1.txt --weir-fst-pop population_2.txt --fst-window-size 10000 --out pop1_vs_pop2_fixed_10000
```


## Ancestry: which scaffolds are derived from both Aibi and A. sp. Kaz?

Let's try to find for each F2 individual whether scaffolds have Aib ancestry (they carry mostly 0/1 SNPs) or not (mostly 0/0 SNPs). We focus only on SNPs that are alternatively fixed between kaz and aib (and use a vcf which already has only F2 individuals):

```
head_filtered_fixedSNP_sexAsex.vcf -> ../3-MakeVCF/head_filtered_fixedSNP_sexAsex.vcf
```

Then we run:

```
perl Chromopaint.pl head_filtered_fixedSNP_sexAsex.vcf
cat head_filtered_fixedSNP_sexAsex.vcf.proportion | grep -v 'NA' >head_filtered_fixedSNP_sexAsex.vcf.proportion.noNA
```

The script Chromopaint.pl is [here](scripts/chromopaint.pl).

It goes through a vcf and counts for each scaffold the number of sites supporting aibi ancestry (0/1 or 1/1) versus only kaz (0/0).
It outputs:
* a file.counts which contains, for each sample/scaffold, the number of aib and kaz SNPs (scaffold 1 sample1_0 sample1_1 sample2_0 sample2_1 etc)
* a file file.proportion which has the proportion of SNPs with aib ancestry for each sample/scaffold. When fewer than 10 informative SNPs were found on a scaffold/sample, the proportion is set to NA.

Before plotting, we select only scaffolds with no "NAs" in the proportions table. **Scaffolds with >80% 0/1 or 1/1 SNPs were considered to be heterozygous for the A. sp. kaz and Aibi haplotypes**, whereas scaffolds with >80% 0/0 were considered to have only A. sp. Kaz ancestry.

The reasoning behind these thresholds is illustrated in Fig. 1 below.

<img src="images/HistogramsAibiAncestry.jpg" width=780>

**Fig. 1:** For each sample, a histogram of the scaffold proportions of 0/1 or 1/1 SNPs. The vast majority of scaffolds in every sample are almost fully 0/0 or almost fully 0/1 or 1/1.
