#!/usr/local/bin/perl

print "make sure you've sorted your input file by contig name!\n";

my $input = $ARGV[0];

open (INPUT, "$input"); 

open (RESULT, ">$input.bestlocation");

$contig0="contig0";
$count=0;
$totscore=0;
$chromname="whatevah";

$c1count=0;
$c1score=0;
@c1coord=();
$c2count=0;
$c2score=0;
@c2coord=();
$c3count=0;
$c3score=0;
@c3coord=();
$c4count=0;
$c4score=0;
@c4coord=();
$c5count=0;
$c5score=0;
@c5coord=();
$c6count=0;
$c6score=0;
@c6coord=();
$c7count=0;
$c7score=0;
@c7coord=();
$c8count=0;
$c8score=0;
@c8coord=();
$c9count=0;
$c9score=0;
@c9coord=();
$c10count=0;
$c10score=0;
@c10coord=();
$c11count=0;
$c11score=0;
@c11coord=();
$c12count=0;
$c12score=0;
@c12coord=();
$c13count=0;
$c13score=0;
@c13coord=();
$c14count=0;
$c14score=0;
@c14coord=();
$c15count=0;
$c15score=0;
@c15coord=();
$c16count=0;
$c16score=0;
@c16coord=();
$c17count=0;
$c17score=0;
@c17coord=();
$c18count=0;
$c18score=0;
@c18coord=();
$c19count=0;
$c19score=0;
@c19coord=();
$c20count=0;
$c20score=0;
@c20coord=();
$c21count=0;
$c21score=0;
@c21coord=();

$othercount=0;
$otherscore=0;
@othercoord=();





while ($line = <INPUT>) {

	chomp $line;

	($contig, $gene, $chrom, $coord, $score) = split(/\s/, $line);

	if ($contig eq $contig0)

		{

		print "the same!\n";

		#find which chrom it is
		#add 1 to that chrom's count
		#add score to that chrom's score

		if ($chrom eq "Chromosome_1") { $c1count++; $c1score=$c1score+$score; push(@c1coord, $coord); }
		elsif ($chrom eq "Chromosome_2") { $c2count++; $c2score=$c2score+$score; push(@c2coord, $coord); }
		elsif ($chrom eq "Chromosome_3") { $c3count++; $c3score=$c3score+$score; push(@c3coord, $coord); }
		elsif ($chrom eq "Chromosome_4") { $c4count++; $c4score=$c4score+$score; push(@c4coord, $coord); }
		elsif ($chrom eq "Chromosome_5") { $c5count++; $c5score=$c5score+$score; push(@c5coord, $coord); }
		elsif ($chrom eq "Chromosome_6") { $c6count++; $c6score=$c6score+$score; push(@c6coord, $coord); }
		elsif ($chrom eq "Chromosome_7") { $c7count++; $c7score=$c7score+$score; push(@c7coord, $coord); }
		elsif ($chrom eq "Chromosome_8") { $c8count++; $c8score=$c8score+$score; push(@c8coord, $coord); }
		elsif ($chrom eq "Chromosome_9") { $c9count++; $c9score=$c9score+$score; push(@c9coord, $coord); }
		elsif ($chrom eq "Chromosome_10") { $c10count++; $c10score=$c10score+$score; push(@c10coord, $coord); }
		elsif ($chrom eq "Chromosome_11") { $c11count++; $c11score=$c11score+$score; push(@c11coord, $coord); }
		elsif ($chrom eq "Chromosome_12") { $c12count++; $c12score=$c12score+$score; push(@c12coord, $coord); }
		elsif ($chrom eq "Chromosome_13") { $c13count++; $c13score=$c13score+$score; push(@c13coord, $coord); }
		elsif ($chrom eq "Chromosome_14") { $c14count++; $c14score=$c14score+$score; push(@c14coord, $coord); }
		elsif ($chrom eq "Chromosome_15") { $c15count++; $c15score=$c15score+$score; push(@c15coord, $coord); }
		elsif ($chrom eq "Chromosome_16") { $c16count++; $c16score=$c16score+$score; push(@c16coord, $coord); }
		elsif ($chrom eq "Chromosome_17") { $c17count++; $c17score=$c17score+$score; push(@c17coord, $coord); }
		elsif ($chrom eq "Chromosome_18") { $c18count++; $c18score=$c18score+$score; push(@c18coord, $coord); }
		elsif ($chrom eq "Chromosome_19") { $c19count++; $c19score=$c19score+$score; push(@c19coord, $coord); }
		elsif ($chrom eq "Chromosome_20") { $c20count++; $c20score=$c20score+$score; push(@c20coord, $coord); }
		elsif ($chrom eq "Chromosome_21") { $c21count++; $c21score=$c21score+$score; push(@c21coord, $coord); }


		else {$othercount++; $otherscore=$otherscore+$score; push(@othercoord, $coord); }			

		$contig0=$contig;
	
	}

	else 



		{
		$countsum=$c1count+$c2count+$c3count+$c4count+$c5count+$c6count+$c7count+$c8count+$c9count+$c10count+$c11count+$c12count+$c13count+$c14count+$c15count+$c16count+$c17count+$c18count+$c19count+$c20count+$c21count+$c22count+$c23count+$c24count+$c25count+$c26count+$c27count+$c28count+$othercount;

		if ($countsum > 0) 

			{

			$count=$c1count;
			$totscore=$c1score;
			$chromname="1";
			@finalcoord = @c1coord;

			if ($c2count == $count) {if ($c2score >= $totscore ) {$count=$c2count; $totscore=$c2score; $chromname="2"; @finalcoord = @c2coord;} else {}} 
			elsif ($c2count > $count) {$count=$c2count; $totscore=$c2score; $chromname="2"; @finalcoord = @c2coord;} 
			else {}

			if ($c3count == $count) {if ($c3score >= $totscore ) {$count=$c3count; $totscore=$c3score; $chromname="3"; @finalcoord = @c3coord;} else {}}
			elsif ($c3count > $count) {$count=$c3count; $totscore=$c3score; $chromname="3";  @finalcoord = @c3coord;} 	
			else {}

			if ($c4count == $count) {if ($c4score >= $totscore ) {$count=$c4count; $totscore=$c4score; $chromname="4"; @finalcoord = @c4coord;} else {}}
			elsif ($c4count > $count) {$count=$c4count; $totscore=$c4score; $chromname="4";  @finalcoord = @c4coord;} 
			else {}

			if ($c5count == $count) {if ($c5score >= $totscore ) {$count=$c5count; $totscore=$c5score; $chromname="5"; @finalcoord = @c5coord;} else {}}
			elsif ($c5count > $count) {$count=$c5count; $totscore=$c5score; $chromname="5";  @finalcoord = @c5coord;} 
			else {}

			if ($c6count == $count) {if ($c6score >= $totscore ) {$count=$c6count; $totscore=$c6score; $chromname="6"; @finalcoord = @c6coord;} else {}}
			elsif ($c6count > $count) {$count=$c6count; $totscore=$c6score; $chromname="6";  @finalcoord = @c6coord;} 
			else {}

			if ($c7count == $count) {if ($c7score >= $totscore ) {$count=$c7count; $totscore=$c7score; $chromname="7"; @finalcoord = @c7coord;} else {}}
			elsif ($c7count > $count) {$count=$c7count; $totscore=$c7score; $chromname="7";  @finalcoord = @c7coord;} 
			else {}

			if ($c8count == $count) {if ($c8score >= $totscore ) {$count=$c8count; $totscore=$c8score; $chromname="8"; @finalcoord = @c8coord;} else {}}
			elsif ($c8count > $count) {$count=$c8count; $totscore=$c8score; $chromname="8";  @finalcoord = @c8coord;} 
			else {}

			if ($c9count == $count) {if ($c9score >= $totscore ) {$count=$c9count; $totscore=$c9score; $chromname="9"; @finalcoord = @c9coord;} else {}}
			elsif ($c9count > $count) {$count=$c9count; $totscore=$c9score; $chromname="9";  @finalcoord = @c9coord;} 
			else {}

			if ($c10count == $count) {if ($c10score >= $totscore ) {$count=$c10count; $totscore=$c10score; $chromname="10"; @finalcoord = @c10coord;} else {}}
			elsif ($c10count > $count) {$count=$c10count; $totscore=$c10score; $chromname="10";  @finalcoord = @c10coord;} 
			else {}

			if ($c11count == $count) {if ($c11score >= $totscore ) {$count=$c11count; $totscore=$c11score; $chromname="11"; @finalcoord = @c11coord;} else {}}
			elsif ($c11count > $count) {$count=$c11count; $totscore=$c11score; $chromname="11";  @finalcoord = @c11coord;} 
			else {}

			if ($c12count == $count) {if ($c12score >= $totscore ) {$count=$c12count; $totscore=$c12score; $chromname="12"; @finalcoord = @c12coord;} else {}}
			elsif ($c12count > $count) {$count=$c12count; $totscore=$c12score; $chromname="12";  @finalcoord = @c12coord;} 
			else {}

			if ($c13count == $count) {if ($c13score >= $totscore ) {$count=$c13count; $totscore=$c13score; $chromname="13"; @finalcoord = @c13coord;} else {}}
			elsif ($c13count > $count) {$count=$c13count; $totscore=$c13score; $chromname="13";  @finalcoord = @c13coord;} 
			else {}

			if ($c14count == $count) {if ($c14score >= $totscore ) {$count=$c14count; $totscore=$c14score; $chromname="14"; @finalcoord = @c14coord;} else {}}
			elsif ($c14count > $count) {$count=$c14count; $totscore=$c14score; $chromname="14";  @finalcoord = @c14coord;} 
			else {}

			if ($c15count == $count) {if ($c15score >= $totscore ) {$count=$c15count; $totscore=$c15score; $chromname="15"; @finalcoord = @c15coord;} else {}}
			elsif ($c15count > $count) {$count=$c15count; $totscore=$c15score; $chromname="15";  @finalcoord = @c15coord;} 
			else {}

			if ($c16count == $count) {if ($c16score >= $totscore ) {$count=$c16count; $totscore=$c16score; $chromname="16"; @finalcoord = @c16coord;} else {}}
			elsif ($c16count > $count) {$count=$c16count; $totscore=$c16score; $chromname="16";  @finalcoord = @c16coord;} 
			else {}

			if ($c17count == $count) {if ($c17score >= $totscore ) {$count=$c17count; $totscore=$c17score; $chromname="17"; @finalcoord = @c17coord;} else {}}
			elsif ($c17count > $count) {$count=$c17count; $totscore=$c17score; $chromname="17";  @finalcoord = @c17coord;} 
			else {}

			if ($c18count == $count) {if ($c18score >= $totscore ) {$count=$c18count; $totscore=$c18score; $chromname="18"; @finalcoord = @c18coord;} else {}}
			elsif ($c18count > $count) {$count=$c18count; $totscore=$c18score; $chromname="18";  @finalcoord = @c18coord;} 
			else {}

			if ($c19count == $count) {if ($c19score >= $totscore ) {$count=$c19count; $totscore=$c19score; $chromname="19"; @finalcoord = @c19coord;} else {}}
			elsif ($c19count > $count) {$count=$c19count; $totscore=$c19score; $chromname="19";  @finalcoord = @c19coord;} 
			else {}

			if ($c20count == $count) {if ($c20score >= $totscore ) {$count=$c20count; $totscore=$c20score; $chromname="20"; @finalcoord = @c20coord;} else {}}
			elsif ($c20count > $count) {$count=$c20count; $totscore=$c20score; $chromname="20";  @finalcoord = @c20coord;} 
			else {}

			if ($c21count == $count) {if ($c21score >= $totscore ) {$count=$c21count; $totscore=$c21score; $chromname="21"; @finalcoord = @c21coord;} else {}}
			elsif ($c21count > $count) {$count=$c21count; $totscore=$c21score; $chromname="21";  @finalcoord = @c21coord;} 
			else {}


			if ($othercount == $count) {if ($otherscore >= $totscore ) {$count=$othercount; $totscore=$otherscore; $chromname="other";} else {}} 
			elsif ($othercount > $count) {$count=$othercount; $totscore=$otherscore; $chromname="other";  @finalcoord = @othercoord;} 
			else {}
			
			#Get mean coordinate for the scaffold

sub average {
my @array = @_; # save the array passed to this function
my $sum; # create a variable to hold the sum of the array's values
foreach (@array) { $sum += $_; } # add each element of the array 
# to the sum
return $sum/@array; # divide sum by the number of elements in the
# array to find the mean
}

			$meancoord = average(@finalcoord);

			print RESULT "$contig0\t$chromname\t$meancoord\t$count\t$totscore\n";

			print "different!\n";

			print "$contig0 $chromname\n";

			#new contig

			#first output results for previous contig, contig0

			print "$contig0 $c1count $c2count $c3count $c4count $c5count $c6count $c7count $c8count $c9count $c10count $c11count $c12count $c13count $c14count $c15count $c16count $c17count $c18count $c19count $c20count $c21count $othercount\n";

			}

	#and empty all the variables we've outputed. 

$count=0;
$totscore=0;
$chromname="whatevah";

$c1count=0;
$c1score=0;
@c1coord=();
$c2count=0;
$c2score=0;
@c2coord=();
$c3count=0;
$c3score=0;
@c3coord=();
$c4count=0;
$c4score=0;
@c4coord=();
$c5count=0;
$c5score=0;
@c5coord=();
$c6count=0;
$c6score=0;
@c6coord=();
$c7count=0;
$c7score=0;
@c7coord=();
$c8count=0;
$c8score=0;
@c8coord=();
$c9count=0;
$c9score=0;
@c9coord=();
$c10count=0;
$c10score=0;
@c10coord=();
$c11count=0;
$c11score=0;
@c11coord=();
$c12count=0;
$c12score=0;
@c12coord=();
$c13count=0;
$c13score=0;
@c13coord=();
$c14count=0;
$c14score=0;
@c14coord=();
$c15count=0;
$c15score=0;
@c15coord=();
$c16count=0;
$c16score=0;
@c16coord=();
$c17count=0;
$c17score=0;
@c17coord=();
$c18count=0;
$c18score=0;
@c18coord=();
$c19count=0;
$c19score=0;
@c19coord=();
$c20count=0;
$c20score=0;
@c20coord=();
$c21count=0;
$c21score=0;
@c21coord=();

$othercount=0;
$otherscore=0;
@othercoord=();

		#now start processing new contig

		$contig0=$contig;
		if ($chrom eq "Chromosome_1") { $c1count++; $c1score=$c1score+$score; push(@c1coord, $coord); }
                elsif ($chrom eq "Chromosome_2") { $c2count++; $c2score=$c2score+$score; push(@c2coord, $coord); }
                elsif ($chrom eq "Chromosome_3") { $c3count++; $c3score=$c3score+$score; push(@c3coord, $coord); }
                elsif ($chrom eq "Chromosome_4") { $c4count++; $c4score=$c4score+$score; push(@c4coord, $coord); }
                elsif ($chrom eq "Chromosome_5") { $c5count++; $c5score=$c5score+$score; push(@c5coord, $coord); }
                elsif ($chrom eq "Chromosome_6") { $c6count++; $c6score=$c6score+$score; push(@c6coord, $coord); }
                elsif ($chrom eq "Chromosome_7") { $c7count++; $c7score=$c7score+$score; push(@c7coord, $coord); }
                elsif ($chrom eq "Chromosome_8") { $c8count++; $c8score=$c8score+$score; push(@c8coord, $coord); }
                elsif ($chrom eq "Chromosome_9") { $c9count++; $c9score=$c9score+$score; push(@c9coord, $coord); }
                elsif ($chrom eq "Chromosome_10") { $c10count++; $c10score=$c10score+$score; push(@c10coord, $coord); }
                elsif ($chrom eq "Chromosome_11") { $c11count++; $c11score=$c11score+$score; push(@c11coord, $coord); }
                elsif ($chrom eq "Chromosome_12") { $c12count++; $c12score=$c12score+$score; push(@c12coord, $coord); }
                elsif ($chrom eq "Chromosome_13") { $c13count++; $c13score=$c13score+$score; push(@c13coord, $coord); }
                elsif ($chrom eq "Chromosome_14") { $c14count++; $c14score=$c14score+$score; push(@c14coord, $coord); }
                elsif ($chrom eq "Chromosome_15") { $c15count++; $c15score=$c15score+$score; push(@c15coord, $coord); }
                elsif ($chrom eq "Chromosome_16") { $c16count++; $c16score=$c16score+$score; push(@c16coord, $coord); }
                elsif ($chrom eq "Chromosome_17") { $c17count++; $c17score=$c17score+$score; push(@c17coord, $coord); }
                elsif ($chrom eq "Chromosome_18") { $c18count++; $c18score=$c18score+$score; push(@c18coord, $coord); }
                elsif ($chrom eq "Chromosome_19") { $c19count++; $c19score=$c19score+$score; push(@c19coord, $coord); }
                elsif ($chrom eq "Chromosome_20") { $c20count++; $c20score=$c20score+$score; push(@c20coord, $coord); }
                elsif ($chrom eq "Chromosome_21") { $c21count++; $c21score=$c21score+$score; push(@c21coord, $coord); }

		else {$othercount++; $otherscore=$otherscore+$score; push(@othercoord, $coord); }			


		}

}


####Process last contig!

		if ($countsum > 0) 

			{

			$count=$c1count;
			$totscore=$c1score;
			$chromname="1";
			@finalcoord = @c1coord;

			if ($c2count == $count) {if ($c2score >= $totscore ) {$count=$c2count; $totscore=$c2score; $chromname="2"; @finalcoord = @c2coord;} else {}} 
			elsif ($c2count > $count) {$count=$c2count; $totscore=$c2score; $chromname="2"; @finalcoord = @c2coord;} 
			else {}

			if ($c3count == $count) {if ($c3score >= $totscore ) {$count=$c3count; $totscore=$c3score; $chromname="3"; @finalcoord = @c3coord;} else {}}
			elsif ($c3count > $count) {$count=$c3count; $totscore=$c3score; $chromname="3";  @finalcoord = @c3coord;} 	
			else {}

			if ($c4count == $count) {if ($c4score >= $totscore ) {$count=$c4count; $totscore=$c4score; $chromname="4"; @finalcoord = @c4coord;} else {}}
			elsif ($c4count > $count) {$count=$c4count; $totscore=$c4score; $chromname="4";  @finalcoord = @c4coord;} 
			else {}

			if ($c5count == $count) {if ($c5score >= $totscore ) {$count=$c5count; $totscore=$c5score; $chromname="5"; @finalcoord = @c5coord;} else {}}
			elsif ($c5count > $count) {$count=$c5count; $totscore=$c5score; $chromname="5";  @finalcoord = @c5coord;} 
			else {}

			if ($c6count == $count) {if ($c6score >= $totscore ) {$count=$c6count; $totscore=$c6score; $chromname="6"; @finalcoord = @c6coord;} else {}}
			elsif ($c6count > $count) {$count=$c6count; $totscore=$c6score; $chromname="6";  @finalcoord = @c6coord;} 
			else {}

			if ($c7count == $count) {if ($c7score >= $totscore ) {$count=$c7count; $totscore=$c7score; $chromname="7"; @finalcoord = @c7coord;} else {}}
			elsif ($c7count > $count) {$count=$c7count; $totscore=$c7score; $chromname="7";  @finalcoord = @c7coord;} 
			else {}

			if ($c8count == $count) {if ($c8score >= $totscore ) {$count=$c8count; $totscore=$c8score; $chromname="8"; @finalcoord = @c8coord;} else {}}
			elsif ($c8count > $count) {$count=$c8count; $totscore=$c8score; $chromname="8";  @finalcoord = @c8coord;} 
			else {}

			if ($c9count == $count) {if ($c9score >= $totscore ) {$count=$c9count; $totscore=$c9score; $chromname="9"; @finalcoord = @c9coord;} else {}}
			elsif ($c9count > $count) {$count=$c9count; $totscore=$c9score; $chromname="9";  @finalcoord = @c9coord;} 
			else {}

			if ($c10count == $count) {if ($c10score >= $totscore ) {$count=$c10count; $totscore=$c10score; $chromname="10"; @finalcoord = @c10coord;} else {}}
			elsif ($c10count > $count) {$count=$c10count; $totscore=$c10score; $chromname="10";  @finalcoord = @c10coord;} 
			else {}

			if ($c11count == $count) {if ($c11score >= $totscore ) {$count=$c11count; $totscore=$c11score; $chromname="11"; @finalcoord = @c11coord;} else {}}
			elsif ($c11count > $count) {$count=$c11count; $totscore=$c11score; $chromname="11";  @finalcoord = @c11coord;} 
			else {}

			if ($c12count == $count) {if ($c12score >= $totscore ) {$count=$c12count; $totscore=$c12score; $chromname="12"; @finalcoord = @c12coord;} else {}}
			elsif ($c12count > $count) {$count=$c12count; $totscore=$c12score; $chromname="12";  @finalcoord = @c12coord;} 
			else {}

			if ($c13count == $count) {if ($c13score >= $totscore ) {$count=$c13count; $totscore=$c13score; $chromname="13"; @finalcoord = @c13coord;} else {}}
			elsif ($c13count > $count) {$count=$c13count; $totscore=$c13score; $chromname="13";  @finalcoord = @c13coord;} 
			else {}

			if ($c14count == $count) {if ($c14score >= $totscore ) {$count=$c14count; $totscore=$c14score; $chromname="14"; @finalcoord = @c14coord;} else {}}
			elsif ($c14count > $count) {$count=$c14count; $totscore=$c14score; $chromname="14";  @finalcoord = @c14coord;} 
			else {}

			if ($c15count == $count) {if ($c15score >= $totscore ) {$count=$c15count; $totscore=$c15score; $chromname="15"; @finalcoord = @c15coord;} else {}}
			elsif ($c15count > $count) {$count=$c15count; $totscore=$c15score; $chromname="15";  @finalcoord = @c15coord;} 
			else {}

			if ($c16count == $count) {if ($c16score >= $totscore ) {$count=$c16count; $totscore=$c16score; $chromname="16"; @finalcoord = @c16coord;} else {}}
			elsif ($c16count > $count) {$count=$c16count; $totscore=$c16score; $chromname="16";  @finalcoord = @c16coord;} 
			else {}

			if ($c17count == $count) {if ($c17score >= $totscore ) {$count=$c17count; $totscore=$c17score; $chromname="17"; @finalcoord = @c17coord;} else {}}
			elsif ($c17count > $count) {$count=$c17count; $totscore=$c17score; $chromname="17";  @finalcoord = @c17coord;} 
			else {}

			if ($c18count == $count) {if ($c18score >= $totscore ) {$count=$c18count; $totscore=$c18score; $chromname="18"; @finalcoord = @c18coord;} else {}}
			elsif ($c18count > $count) {$count=$c18count; $totscore=$c18score; $chromname="18";  @finalcoord = @c18coord;} 
			else {}

			if ($c19count == $count) {if ($c19score >= $totscore ) {$count=$c19count; $totscore=$c19score; $chromname="19"; @finalcoord = @c19coord;} else {}}
			elsif ($c19count > $count) {$count=$c19count; $totscore=$c19score; $chromname="19";  @finalcoord = @c19coord;} 
			else {}

			if ($c20count == $count) {if ($c20score >= $totscore ) {$count=$c20count; $totscore=$c20score; $chromname="20"; @finalcoord = @c20coord;} else {}}
			elsif ($c20count > $count) {$count=$c20count; $totscore=$c20score; $chromname="20";  @finalcoord = @c20coord;} 
			else {}

			if ($c21count == $count) {if ($c21score >= $totscore ) {$count=$c21count; $totscore=$c21score; $chromname="21"; @finalcoord = @c21coord;} else {}}
			elsif ($c21count > $count) {$count=$c21count; $totscore=$c21score; $chromname="21";  @finalcoord = @c21coord;} 
			else {}


			if ($othercount == $count) {if ($otherscore >= $totscore ) {$count=$othercount; $totscore=$otherscore; $chromname="other";} else {}} 
			elsif ($othercount > $count) {$count=$othercount; $totscore=$otherscore; $chromname="other";  @finalcoord = @othercoord;} 
			else {}
			
			#Get mean coordinate for the scaffold

sub average {
my @array = @_; # save the array passed to this function
my $sum; # create a variable to hold the sum of the array's values
foreach (@array) { $sum += $_; } # add each element of the array 
# to the sum
return $sum/@array; # divide sum by the number of elements in the
# array to find the mean
}

			$meancoord = average(@finalcoord);

			print RESULT "$contig0\t$chromname\t$meancoord\t$count\t$totscore\n";

			print "different!\n";

			print "$contig0 $chromname\n";

			#new contig

			#first output results for previous contig, contig0

			print "$contig0 $c1count $c2count $c3count $c4count $c5count $c6count $c7count $c8count $c9count $c10count $c11count $c12count $c13count $c14count $c15count $c16count $c17count $c18count $c19count $c20count $c21count $othercount\n";

			}


