## 1. Transform raw sequencing files with rare male and its sister's DNA from bam into fq format.

working directory: `/nfs/scistore03/vicosgrp/ukhaurat/raremale_cov` 

```
module load picard
cd /nfs/scistore03/vicosgrp/ukhaurat/raremale_cov/

java -jar $PICARD SamToFastq I=asexualsister_Aibi.bam F=asexualsister_Aibi_1.fq F2=asexualsister_2.fq

java -jar $PICARD SamToFastq I=raremale_Aibi.bam F=raremale_Aibi_1.fq F2=raremale_2.fq
```

## 2. Trim raw sequencing fq files.  

```
module load trimmomatic

srun java -jar /nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 4 asexualsister_Aibi_1.fq asexualsister_Aibi_2.fq asexualsister_Aibi_1_trimmed.fq asexualsister_Aibi_1_un.trimmed.fq asexualsister_Aibi_2_trimmed.fq asexualsister_Aibi_2_un.trimmed.fq ILLUMINACLIP:/nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 SLIDINGWINDOW:4:22 MINLEN:36 LEADING:3 TRAILING:3

srun java -jar /nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 4 raremale_Aibi_1.fq raremale_Aibi_2.fq raremale_Aibi_1_trimmed.fq raremale_Aibi_1_un.trimmed.fq raremale_Aibi_2_trimmed.fq raremale_Aibi_2_un.trimmed.fq ILLUMINACLIP:/nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 SLIDINGWINDOW:4:22 MINLEN:36 LEADING:3 TRAILING:3
```

## 3. Map raremale DNA and its asexual sister to the full A.sinica genome.   

working directory: `/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/`

```
module load star

STAR --runMode genomeGenerate --genomeDir index --genomeFastaFiles k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq --runThreadN 40 --genomeChrBinNbits 10

STAR --genomeDir ./index --readFilesIn raremale_Aibi_1_trimmed.fq raremale_Aibi_2_trimmed.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix raremale_Aibi_ --runThreadN 40

STAR --genomeDir ./index --readFilesIn asexualsister_Aibi_1_trimmed.fq asexualsister_Aibi_2_trimmed.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix asexsister_Aibi_ --runThreadN 40
```

## 4. SNP calling and VCF creation. 

with the less strict filtering for coverage (>5,<100). 

```
module load samtools
module load bcftools
module load vcftools

#Call SNPs from the BAM alignments
srun bcftools mpileup -a AD,DP,SP -Ou --threads 40 -f k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq asexsister_Aibi_Aligned.sortedByCoord.out.bam raremale_Aibi_Aligned.sortedByCoord.out.bam | bcftools call -v -f GQ,GP -mO z -o head_asex_raremale.vcf.gz

#filter for quality and coverage
srun vcftools --gzvcf head_asex_raremale.vcf.gz --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 5 --max-meanDP 100 --minDP 5 --maxDP 100 --recode --stdout > head_asex_raremale_cov5_filtered.vcf

#filter2:remove multiallelic
bcftools view --max-alleles 2 --exclude-types indels head_asex_raremale_cov5_filtered.vcf > head_asex_raremale_cov5_filtered2.vcf
```
We then clean the vcf file by removing the extra information on top:
```
grep -v "##" head_asex_raremale_cov5_filtered2.vcf > head_asex_raremale_cov5_filtered2_clean_kaz_genome.vcf
```
To identify the SNPs that lost heterozygosity on Chromosome 1, we use the following python commands (the file AkazScaf_AsinChromLocation.txt was produced using the steps described [here](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/bestlocation.md)):
```
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy
import os
loh=pd.read_csv("head_asex_raremale_cov5_filtered2_clean_kaz_genome.vcf",sep="\t")
loh[['GTs','PLs','DPs','SPs','ADs','GPs','GQs']]=loh['asexsister_Aibi_Aligned.sortedByCoord.out.bam'].str.split(':',expand=True)
loh[['GTb','PLb','DPb','SPb','ADb','GPb','GQb']]=loss_new_notsimple['raremale_Aibi_Aligned.sortedByCoord.out.bam'].str.split(':',expand=True)
col_n=['scaffold','LG','Location','Support','Match']
akaz_vs_asin=pd.read_csv("AkazScaf_AsinChromLocation.txt",sep="\t",header=None,names=col_n)
akaz_vs_asin_2=akaz_vs_asin[(akaz_vs_asin['LG']=="1") & (akaz_vs_asin['Support']>=1)]
merged= pd.merge(left=akaz_vs_asin_2, right=loh, left_on='scaffold', right_on='#CHROM').sort_values(by=['Location'])
loh_3=merged[((merged['GTs']=='0/1') & (merged['GTb']=='0/0')) | ((merged['GTs']=='0/1') & (merged['GTb']=='1/1'))]
x1,y1,_=plt.hist(loh_3['Location'],bins=np.arange(0,102000000,500000)) #histogram of SNPs that lost heterozygosity
x2,y2,_=plt.hist(merged['Location'],bins=np.arange(0,102000000,500000)) #histogram of all SNPs
```
