I used Flye with following commands to make the female genome assembly:
```
module load python/3

python /nfs/scistore03/vicosgrp/melkrewi/Flye/Flye/bin/flye --pacbio-raw Fpacbio.fasta --genome-size 1g --threads 20 --out-dir ./assembly
```
The resulting assembly was labeled assembly.fasta and can be found here

In order to check the contiguity of the assembly I used:
```
module load assembly-stats
assembly-stats assembly.fasta
```
The results for the female:
- sum = 1630973376, n = 14340, ave = 113735.94, largest = 2484262
- N50 = 373245, n = 1236
- N60 = 283427, n = 1739
- N70 = 202128, n = 2419
- N80 = 131545, n = 3411
- N90 = 66433, n = 5147
- N100 = 204, n = 14340
- N_count = 9400
- Gaps = 94

## W-specific scaffolds:

Artemia sinica female specific kmers were identified as described [here](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/Identification_of_candidate_W-derived_transcripts.md)

The female specific kmers the were mapped to the assembly using Bowtie2:
```
module load bowtie2/2.4.4

srun bowtie2-build female_assembly.fasta Asinica_w

srun bowtie2 -p 40 --no-unal --no-hd --no-sq -x Asinica_w -f v_female_specific_mers.fasta -S female_1.sam

awk '($6=="31M")' female_1.sam | grep 'NM:i:0' > female_1_perfectmatch.sam

grep '>' female_assembly.fasta | perl -pi -e 's/>//gi' | perl -pi -e 's/ .*//gi' > genome.list

cat female_1_perfectmatch.sam | cut -f 3 | cat /dev/stdin genome.list | sort | uniq -c | awk '{print $2, $1-1}' > female_1_perfectmatch.counts

cat female_1.sam | cut -f 3 | cat /dev/stdin genome.list | sort | uniq -c | awk '{print $2, $1-1}' > female_1.counts
```
The Male and Female genomic reads were mapped to the assembly as well with the following commands:
```
module load bowtie2/2.4.4

srun bowtie2-build female_assembly.fasta Asinica_w

srun bowtie2 -p 40 --no-unal --no-hd --no-sq -x Asinica_w -U 38745_2_1.fastq -S female_1.sam

srun bowtie2 -p 40 --no-unal --no-hd --no-sq -x Asinica_w -U 38745_5_1.fastq -S female_2.sam

srun bowtie2 -p 40 --no-unal --no-hd --no-sq -x Asinica_w -U 38744_2_1.fastq -S male_1.sam

srun bowtie2 -p 40 --no-unal --no-hd --no-sq -x Asinica_w -U 38744_5_1.fastq -S male_2.sam

srun bowtie2 -p 40 --no-unal --no-hd --no-sq -x Asinica_w -U CC2U8ANXX_5_1.fastq.gz -S female_3.sam

srun bowtie2 -p 40 --no-unal --no-hd --no-sq -x Asinica_w -U CC2U8ANXX_4_1.fastq.gz -S male_3.sam

awk '($6=="125M")' female_1.sam | grep 'NM:i:0' > female_1_perfectmatch.sam

awk '($6=="125M")' female_2.sam | grep 'NM:i:0' > female_2_perfectmatch.sam

awk '($6=="125M")' female_3.sam | grep 'NM:i:0' > female_3_perfectmatch.sam

awk '($6=="125M")' male_1.sam | grep 'NM:i:0' > male_1_perfectmatch.sam

awk '($6=="125M")' male_2.sam | grep 'NM:i:0' > male_2_perfectmatch.sam

awk '($6=="125M")' male_3.sam | grep 'NM:i:0' > male_3_perfectmatch.sam

grep '>' female_assembly.fasta | perl -pi -e 's/>//gi' | perl -pi -e 's/ .*//gi' > transcripts.list

cat female_1_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > female_1_perfectmatch.counts

cat female_2_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > female_2_perfectmatch.counts

cat female_3_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > female_3_perfectmatch.counts

cat male_1_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > male_1_perfectmatch.counts

cat male_2_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > male_2_perfectmatch.counts

cat male_3_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > male_3_perfectmatch.counts
```
