# Pipeline used to obtain male and female genomic coverage in A. sinica

## Read mapping

First we indexed the chromosome level assembly genome of A. sinica

```
bowtie2-build Chromosomal_level_Asinica_assembly.fa AsinicaChromOnly_LA
```

Then we mapped the A. sinica reads to the indexed reference genome
```
A. sinica Female colony
bowtie2 -x AsinicaChromOnly_LA -1 38745F_5_1.fastq -2 38745F_5_2.fastq --end-to-end --sensitive -p 8 -S 38745CLAFRR_5.sam
bowtie2 -x AsinicaChromOnly_LA -1 38745F_2_1.fastq -2 38745F_2_2.fastq --end-to-end --sensitive -p 8 -S 38745CLAFRR_2.sam

A. sinica Isofemale
bowtie2 -x AsinicaChromOnly_LA -1 CC2U8ANXXF_5_1.fastq -2 CC2U8ANXXF_5_2.fastq --end-to-end --sensitive -p 8 -S CC2U8ANXXCLAFRR_5.sam

A. sinica Male colony
bowtie2 -x AsinicaChromOnly_LA -1 38744M_2_1.fastq -2 38744M_2_2.fastq --end-to-end --sensitive -p 8 -S 38744CLAMRR_2.sam
bowtie2 -x AsinicaChromOnly_LA -1 38744M_5_1.fastq -2 38744M_5_2.fastq --end-to-end --sensitive -p 8 -S 38744CLAMRR_5.sam

A. sinica Isomale 
bowtie2 -x AsinicaChromOnly_LA -1 CC2U8ANXXM_4_1.fastq -2 CC2U8ANXXM_4_2.fastq --end-to-end --sensitive -p 8 -S CC2U8ANXXCLAMRR_4.sam
```

We then extracted unique reads from the mapped libraries

```
grep -vw "XS:i" 38744CLAMRR_5.sam > 38744CLAMRR_5_unique.sam
grep -vw "XS:i" 38744CLAMRR_2.sam > 38744CLAMRR_2_unique.sam
grep -vw "XS:i" 38745CLAFRR_2.sam > 38745CLAFRR_2_unique.sam
grep -vw "XS:i" 38745CLAFRR_5.sam > 38745CLAFRR_5_unique.sam
grep -vw "XS:i" CC2U8ANXXCLAMRR_4.sam > CC2U8ANXXCLAMRR_4_unique.sam
grep -vw "XS:i" CC2U8ANXXCLAFRR_5.sam > CC2U8ANXXCLAFRR_5_unique.sam
```

## Coverage estimation

We computed the DNA coverage for each mapped library using soapcoverage. We specified the sliding window of length 10kbs

```
soap.coverage -sam -cvg -i 38744CLAMRR_2_unique.sam -onlyuniq -p 8 -refsingle Chromosomal_level_Asinica_assembly.fa -window 38744CLAMRR_2_window 10000

soap.coverage -sam -cvg -i 38744CLAMRR_5_unique.sam -onlyuniq -p 8 -refsingle Chromosomal_level_Asinica_assembly.fa -window 38744CLAMRR_5_window 10000

soap.coverage -sam -cvg -i 38745CLAFRR_2_unique.sam -onlyuniq -p 8 -refsingle Chromosomal_level_Asinica_assembly.fa -window 38745CLAFRR_2_window 10000

soap.coverage -sam -cvg -i 38745CLAFRR_5_unique.sam -onlyuniq -p 8 -refsingle Chromosomal_level_Asinica_assembly.fa -window 38745CLAFRR_5_window 10000

soap.coverage -sam -cvg -i CC2U8ANXXCLAMRR_4_unique.sam -onlyuniq -p 8 -refsingle Chromosomal_level_Asinica_assembly.fa -window CC2U8ANXXCLAMRR_4_window 10000

soap.coverage -sam -cvg -i CC2U8ANXXCLAFRR_5_unique.sam -onlyuniq -p 8 -refsingle Chromosomal_level_Asinica_assembly.fa -window CC2U8ANXXCLAFRR_5_window 10000
```
