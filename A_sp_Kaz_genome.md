# Artemia sp. Kazakhstan draft genome assembly.

## Megahit:

```
srun ~/megahit -1 168700_S5_L002_R1_001.fastq.gz -2 168700_S5_L002_R2_001.fastq.gz -1 168701_S6_L002_R1_001.fastq.gz -2 168701_S6_L002_R2_001.fastq.gz -o Assembly_mega -t 40
```

## SOAPdenovo-fusion
```
module load SOAPdenovo2
#run commands on SLURM's srun
srun SOAPdenovo-fusion -D -s soapconfig.txt -p 30 -K 41 -g k41_scaffolds -c final.contigs.fa
srun SOAPdenovo-127mer map -s soapconfig.txt -p 30 -g k41_scaffolds
srun SOAPdenovo-127mer scaff -p 35 -g k41_scaffolds
```
The config file is here:
```
#maximal read length
max_rd_len=150
[LIB]
avg_ins=406
reverse_seq=0
asm_flags=3
# cutoff of pair number for a reliable connection (at least 3 for short insert size)
pair_num_cutoff=3
#minimum aligned length to contigs for a reliable read location (at least 32 for short insert size)
map_len=32
#a pair of fastq file, read 1 file should always be followed by read 2 file
q1=168700_S5_L002_R1_001.fastq.gz
q2=168700_S5_L002_R2_001.fastq.gz
[LIB]
avg_ins=432
reverse_seq=0
asm_flags=3
# cutoff of pair number for a reliable connection (at least 3 for short insert size)
pair_num_cutoff=3
#minimum aligned length to contigs for a reliable read location (at least 32 for short insert size)
map_len=32
#a pair of fastq file, read 1 file should always be followed by read 2 file
q1=168701_S6_L002_R1_001.fastq.gz
q2=168701_S6_L002_R2_001.fastq.gz
```
