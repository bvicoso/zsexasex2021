# Key pipelines used in Elkrewi, Khauratovich, Toups et al. (2022)

* [Genome assembly and scaffolding into Chromosomes](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/Artemia_sinica_genome_assembly.md)
* [Female assembly and identification of candidate W-linked genome scaffolds](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/Female_Genome_Assembly.md)
* [Male and female coverage estimation in A. sinica](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/MFcov_Asinica.md) 
* [Artemia sp. Kazakhstan draft genome assembly](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/A_sp_Kaz_genome.md) 
* [Coverage analysis in A. sp. Kazakhstan, A. urmiana, asexual females and the rare male (A. parthenogenetica Aibi Lake)](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/MF_covAkazakh_and_asexuals.md)
* [Mapping of the A. franciscana and A. sp. Kazakhstan genomes to the new A. sinica assembly](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/bestlocation.md) 
* [SNP calling and Fst between males and females using RNA-seq data](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/fst.md)
* [Identification of candidate W-derived transcripts](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/Identification_of_candidate_W-derived_transcripts.md)
* [Transcriptome Assemblies and Gene expression analysis](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/Artemia_sinica_Transcriptome_Assembly_and_Gene_expression_analysis.md)
* [Phylogenetic Trees](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/Alignments_and_trees.md)
* [SNP calling in asexual female and rare male, and changes in heterozygosity between them](Heterozygosity_rare_male.md)
* [SNP calling in backcrossing experiment of rare male and Fst between F2 asexual females and control](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/SNP_calling_in_backcrossing_experiment.md)
* [Inference of genomic regions inherited by all F2 asexual females from the Aibi Lake rare male in the backcrossing experiment.](ancestrySNP.md)

