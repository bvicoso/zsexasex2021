# Assign a location on the Asinica genome to A.fran/A.kaz scaffolds based on their gene content

## Quick summary of output

The final file **AkazScaf_AsinChromLocation.txt**  has the following columns:
* scaffold from outgroup species to be assigned to a chromosomal location
* chromosome in the reference species
* location on the chromosome
* number of genes/transcripts that supported this assignment
* total match score of genes that supported this assignment

## Map transcripts to the A. sinica genome assembly

We map transcripts of A. sinica to the genome with pblat and only keep the location with the highest mapping score for each transcript:

```
pblat -minScore=50 Artemia_sinica_genome_29_12_2021.fasta ArtemiaSinica_EviGene_assembly.okay.cds genome_vs_trans.blat -t=dnax -q=dnax -threads=50

sort -k 10 genome_vs_trans.blat > genome_vs_trans.blat.sorted

perl 1-besthitblat.pl genome_vs_trans.blat.sorted
```

We Keep only transcripts longer than 500bps in the A. sinica transcriptome:
```
module load fafilter/v.357
faFilter -minSize=500 ArtemiaSinica_EviGene_assembly.okay.cds ArtemiaSinica_EviGene_assembly.over500bps.cds
```
We map the transcripts of A. sinica against the A. franciscana and A. sp. Kazakstan genomic scaffolds with pblat (the example is for A. sp. Kazakhstan):

```
module load pblat
pblat -minScore=50 k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq ArtemiaSinica_EviGene_assembly.over500bps.cds kgenome_vs_trans.blat -t=dnax -q=dnax -threads=40
```

The we  keep only the best hit per A. sinica transcript in the Afran/Akaz genome, but also remove redundant transcripts (ie if multiple transcripts map to the same location keep only the one with the best hit).
```
sort -k 10 kgenome_vs_trans.blat > kgenome_vs_trans.blat.sorted
perl 1-besthitblat.pl kgenome_vs_trans.blat.sorted
sort -k 14 kgenome_vs_trans.blat.sorted.besthit > kgenome_vs_trans.sortedbyDB
perl 2-redremov_blat_v2.pl kgenome_vs_trans.sortedbyDB
```

The script 1-besthitblat.pl is [here](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/1-besthitblat.pl). 

The script redremov_blat_v2.pl is [here](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/2-redremov_blat_v2.pl).

The script AssignScaffoldLocation.pl is [here](https://git.ist.ac.at/bvicoso/zsexasex2021/-/blob/master/AssignScaffoldLocation_2.pl)

## Script to assign best location

Usage:
```
perl AssignScaffoldLocation.pl inputfile
```

The input file should be in the following format (sorted by contig):
```
	($contig, $gene, $chrom, $coord, $score) = split(/\s/, $line);
```
Where the different columns are:
* contig = contig/scaffold from outgroup species to be assigned to a location
* gene = gene/transcript from reference species
* chrom = chromosome of gene/transcript in reference species
* coord = midpoint of gene on the chromosome in the reference species (mean of start and end of blat match for instance)
* score = match score of gene to the scaffold to be assigned to a location

## Run pipeling to assign best location on chromosomes

### Make input file 
Select only important information from the A. sinica transcript to genome blat file:
```
cat genome_vs_trans.blat.sorted.besthit | awk '($11>500)' | awk '{print $10,$14,($16+$17)/2,$1}' | sort > Asinica_GeneGenomeLocation_score.sorted
```
Select only important information from ReferenceTranscript to OutgroupGenome blat:
```
cat kgenome_vs_trans.sortedbyDB.nonredundant | awk '{print $10, $14, $1}' | sort > AsinTranscripts500_vs_AkazGenome.joinable
```
Merge files:
```
join AsinTranscripts500_vs_AkazGenome.joinable Asinica_GeneGenomeLocation_score.sorted | awk '{print $2, $1, $4, $5, $3}' | sort > AkazScaf_inputforLocAssign.txt
```

### Assign location

```
perl AssignScaffoldLocation.pl AkazScaf_inputforLocAssign.txt
mv AkazScaf_inputforLocAssign.txt.bestlocation AkazScaf_AsinChromLocation.txt
```

The final file **AfranScaf_AsinChromLocation.txt** has the following columns:
* scaffold from outgroup species to be assigned to a chromosomal location
* chromosome in the reference species
* location on the chromosome
* number of genes/transcripts that supported this assignment
* total match score of genes that supported this assignment


