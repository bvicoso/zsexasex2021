# Subset reads for the pooled analysis

Let's make sure we start with the same number of reads per female in each pool (asexuals and controls).

WD: /nfs/scistore18/vicosgrp/bvicoso/Artemia_FST_Melissa/KazRef/1b-ReadSubset

Get reads:

```
ln -s ../1-Reads/*[0-9].fq[12] .
```

The smallest number of reads that we have for any female is: 40676997
So let's subset each sample to that number. 

```
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex1.fq1 40676997 > asex1_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex1.fq2 40676997 > asex1_sub.fq2
```

<details><summary>The rest of the commands are hidden here.</summary>

```
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex2.fq1 40676997 > asex2_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex2.fq2 40676997 > asex2_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex3.fq1 40676997 > asex3_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex3.fq2 40676997 > asex3_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex4.fq1 40676997 > asex4_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex4.fq2 40676997 > asex4_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex5.fq1 40676997 > asex5_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 asex5.fq2 40676997 > asex5_sub.fq2

##Controls

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex1.fq1 40676997 > sex1_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex1.fq2 40676997 > sex1_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex2.fq1 40676997 > sex2_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex2.fq2 40676997 > sex2_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex3.fq1 40676997 > sex3_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex3.fq2 40676997 > sex3_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex4.fq1 40676997 > sex4_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex4.fq2 40676997 > sex4_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex5.fq1 40676997 > sex5_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex5.fq2 40676997 > sex5_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex6.fq1 40676997 > sex6_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex6.fq2 40676997 > sex6_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex7.fq1 40676997 > sex7_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex7.fq2 40676997 > sex7_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex8.fq1 40676997 > sex8_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex8.fq2 40676997 > sex8_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex9.fq1 40676997 > sex9_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex9.fq2 40676997 > sex9_sub.fq2

/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex10.fq1 40676997 > sex10_sub.fq1
/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk sample -s100 sex10.fq2 40676997 > sex10_sub.fq2
```

</details>


To check that we recovered the same set of forward and reverse reads:

```
grep '@A00' asex1_sub.fq2 | perl -pi -e 's/\/[12]//gi' | md5sum
grep '@A00' asex1_sub.fq1 | perl -pi -e 's/\/[12]//gi' | md5sum
```
