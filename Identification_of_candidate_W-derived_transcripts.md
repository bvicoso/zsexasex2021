## Artemia sinica Kmer pipeline:
```
module load bbmap

kmercountexact.sh k=31 in1=CC2U8ANXX_5_1.fastq.gz in2=CC2U8ANXX_5_2.fastq.gz out=sfemale_mer_sinica.fa

cat 38745_2_1.fastq 38745_5_1.fastq > 38745_1.fastq

cat 38745_2_2.fastq 38745_5_2.fastq > 38745_2.fastq

kmercountexact.sh k=31 in1=38745_1.fastq in2=38745_2.fastq out=sfemale_mer_sinica_2.fa

cat 39871_1.fastq 39872_1.fastq > 3987_1.fastq

cat 39871_2.fastq 39872_2.fastq > 3987_2.fastq

kmercountexact.sh k=31 in1=3987_1.fastq in2=3987_2.fastq out=sfemale_rna_mer.fa -Xmx350g

kmercountexact.sh k=31 in=sfemale_mer_sinica.fa,sfemale_mer_sinica_2.fa,sfemale_rna_mer.fa out=shared_female_mer.fa mincount=3 -Xmx500g

bbduk.sh k=31 in=shared_female_mer.fa out=female_specific_mers.fasta ref=CC2U8ANXX_4_1.fastq.gz,CC2U8ANXX_4_2.fastq.gz -Xmx500g

bbduk.sh k=31 in=female_specific_mers.fasta out=v_female_specific_mers.fasta ref=39869_1.fastq,39869_2.fastq,39870_1.fastq,39870_2.fastq

bbduk.sh k=31 in=v_female_specific_mers.fasta out=v_v_female_specific_mers.fasta ref=38744_2_1.fastq,38744_2_2.fastq,38744_5_1.fastq,38744_5_2.fastq -Xmx500g

bbduk.sh k=31 in1=39872_1.fastq in2=39872_2.fastq outm1=female_specific_rna_reads_1_1.fastq outm2=female_specific_rna_reads_1_2.fastq ref=v_v_female_specific_mers.fasta mkf=0.6

bbduk.sh k=31 in1=39871_1.fastq in2=39871_2.fastq outm1=female_specific_rna_reads_2_1.fastq outm2=female_specific_rna_reads_2_2.fastq ref=v_v_female_specific_mers.fasta mkf=0.6
cat female_specific_rna_reads_1_1.fastq female_specific_rna_reads_2_1.fastq > female_specific_rna_reads_1.fastq

cat female_specific_rna_reads_1_2.fastq female_specific_rna_reads_2_2.fastq > female_specific_rna_reads_2.fastq
```
## Filtering Artemia sinica
```
module load java

module load bowtie2/2.4.4

module load soap/coverage

srun bowtie2-build Trinity_longest_sinica_02_04_2022_renamed_clean.fasta Asinica_w

srun bowtie2 -p 16 --no-unal --no-hd --no-sq -x Asinica_w -U 38745_2_1.fastq -S female_1.sam

srun bowtie2 -p 16 --no-unal --no-hd --no-sq -x Asinica_w -U 38745_5_1.fastq -S female_2.sam

srun bowtie2 -p 16 --no-unal --no-hd --no-sq -x Asinica_w -U 38744_2_1.fastq -S male_1.sam

srun bowtie2 -p 16 --no-unal --no-hd --no-sq -x Asinica_w -U 38744_5_1.fastq -S male_2.sam

srun bowtie2 -p 16 --no-unal --no-hd --no-sq -x Asinica_w -U CC2U8ANXX_5_1.fastq.gz -S female_3.sam

srun bowtie2 -p 16 --no-unal --no-hd --no-sq -x Asinica_w -U CC2U8ANXX_4_1.fastq.gz -S male_3.sam

awk '($6=="125M")' female_1.sam | grep 'NM:i:0' > female_1_perfectmatch.sam

awk '($6=="125M")' female_2.sam | grep 'NM:i:0' > female_2_perfectmatch.sam

awk '($6=="125M")' female_3.sam | grep 'NM:i:0' > female_3_perfectmatch.sam

awk '($6=="125M")' male_1.sam | grep 'NM:i:0' > male_1_perfectmatch.sam

awk '($6=="125M")' male_2.sam | grep 'NM:i:0' > male_2_perfectmatch.sam

awk '($6=="125M")' male_3.sam | grep 'NM:i:0' > male_3_perfectmatch.sam

grep '>' Trinity_longest_sinica_02_04_2022_renamed_clean.fasta | perl -pi -e 's/>//gi' | perl -pi -e 's/ .*//gi' > transcripts.list

cat female_1_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > female_1_perfectmatch.counts

cat female_2_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > female_2_perfectmatch.counts

cat female_3_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > female_3_perfectmatch.counts

cat male_1_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > male_1_perfectmatch.counts

cat male_2_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > male_2_perfectmatch.counts

cat male_3_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > male_3_perfectmatch.counts
```

## Artemia franciscana:
```
module load bbmap

cat 60545_1.fastq 60546_1.fastq 60547_1.fastq 60548_1.fastq > female_pool_1.fastq

cat 60545_2.fastq 60546_2.fastq 60547_2.fastq 60548_2.fastq > female_pool_2.fastq

kmercountexact.sh k=31 in1=CC2U_7_1.fastq in2=CC2U_7_2.fastq out=sfemale_mer_fran.fa

kmercountexact.sh k=31 in1=female_pool_1.fastq in2=female_pool_2.fastq out=sfemale_rna_mer.fa

kmercountexact.sh k=31 in1=SRR14598204_1.fastq in2=SRR14598204_2.fastq out=sfemale_rna_mer_2.fa

kmercountexact.sh k=31 in=sfemale_mer_fran.fa,sfemale_rna_mer.fa,sfemale_rna_mer_2.fa out=shared_female_mer.fa mincount=3 -Xmx500g

bbduk.sh k=31 in=shared_female_mer.fa out=female_specific_mers.fasta ref=CC2U_6_1.fastq,CC2U_6_2.fastq -Xmx500g

bbduk.sh k=31 in=female_specific_mers.fasta out=v_female_specific_mers.fasta ref=SRR14598203_1.fastq,SRR14598203_2.fastq -Xmx350G

bbduk.sh k=31 in=v_female_specific_mers.fasta out=v_v_female_specific_mers.fasta ref=60544_1.fastq,60544_2.fastq,60543_1.fastq,60543_2.fastq,60542_1.fastq,60542_2.fastq,60541_1.fastq,60541_2.fastq -Xmx500g

bbduk.sh k=31 in1=female_pool_1.fastq in2=female_pool_2.fastq outm1=female_specific_rna_reads_1_1.fastq outm2=female_specific_rna_reads_1_2.fastq ref=v_v_female_specific_mers.fasta mkf=0.6

bbduk.sh k=31 in1=SRR14598204_1.fastq in2=SRR14598204_2.fastq outm1=female_specific_rna_reads_2_1.fastq outm2=female_specific_rna_reads_2_2.fastq ref=v_v_female_specific_mers.fasta mkf=0.6

cat female_specific_rna_reads_1_1.fastq female_specific_rna_reads_2_1.fastq > female_specific_rna_reads_1.fastq

cat female_specific_rna_reads_1_2.fastq female_specific_rna_reads_2_2.fastq > female_specific_rna_reads_2.fastq
```
## Filtering Artemia franciscana
```
module load java

module load bowtie2/2.4.4

module load soap/coverage

srun bowtie2-build Trinity_longest_fran_02_04_2022_renamed_clean.fasta Afran_w

srun bowtie2 -p 16 --no-unal --no-hd --no-sq -x Afran_w -U CC2U_7_1.fastq -S female_1.sam

srun bowtie2 -p 16 --no-unal --no-hd --no-sq -x Afran_w -U CC2U_6_1.fastq -S male_1.sam

awk '($6=="125M")' female_1.sam | grep 'NM:i:0' > female_1_perfectmatch.sam

awk '($6=="125M")' male_1.sam | grep 'NM:i:0' > male_1_perfectmatch.sam

grep '>' Trinity_longest_fran_02_04_2022_renamed_clean.fasta | perl -pi -e 's/>//gi' | perl -pi -e 's/ .*//gi' > transcripts.list

cat female_1_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > female_1_perfectmatch.counts

cat male_1_perfectmatch.sam | cut -f 3 | cat /dev/stdin transcripts.list | sort | uniq -c | awk '{print $2, $1-1}' > male_1_perfectmatch.counts
```
