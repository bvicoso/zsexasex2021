# Pipeline used to obtain male and female genomic coverage in A. sp. Kazkhstan

## Read mapping

First we indexed the chromosome level assembly genome of A. sinica

```
srun bowtie2-build k41_scaffolds.scafSeq KazakhstanGenome_Scaffold
```

Then we mapped the A. sp. Kazkhstan reads to the indexed reference genome
```
srun bowtie2 -x KazakhstanGenome_Scaffold -1 168700_1.fastq -2 168700_2.fastq --end-to-end --sensitive -p 8 -S 168700M_1.sam
srun bowtie2 -x KazakhstanGenome_Scaffold -1 168701_1.fastq -2 168701_2.fastq --end-to-end --sensitive -p 8 -S 168701M_1.sam
srun bowtie2 -x KazakhstanGenome_Scaffold -1 168702_1.fastq -2 168702_2.fastq --end-to-end --sensitive -p 8 -S 168702F_1.sam
srun bowtie2 -x KazakhstanGenome_Scaffold -1 38743KF_2_1.fastq -2 38743KF_2_2.fastq --end-to-end --sensitive -p 8 -S 38743KF_2.sam
srun bowtie2 -x KazakhstanGenome_Scaffold -1 38743KF_5_1.fastq -2 38743KF_5_2.fastq --end-to-end --sensitive -p 8 -S 38743KF_5.sam
```

We then extracted unique reads from the mapped libraries

```
srun grep -vw "XS:i" 168700M_1.sam > 168700M_1_unique.sam
srun grep -vw "XS:i" 168701M_1.sam > 168701M_1_unique.sam
srun grep -vw "XS:i" 168702F_1.sam > 168702F_1_unique.sam
srun grep -vw "XS:i" 38743KF_2.sam > 38743KF_2_unique.sam
srun grep -vw "XS:i" 38743KF_5.sam > 38743KF_5_unique.sam
```

## Coverage estimation

We computed the DNA coverage for each mapped library using soapcoverage. 

```
srun soap.coverage -sam -cvg -i 168700M_1_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 168700M_1_unique.soapcov

srun soap.coverage -sam -cvg -i 168701M_1_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 168701M_1_unique.soapcov

srun soap.coverage -sam -cvg -i 168702F_1_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 168702F_1_unique.soapcov

srun soap.coverage -sam -cvg -i 38743KF_2_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 38743KF_2_unique.soapcov

srun soap.coverage -sam -cvg -i 38743KF_5_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 38743KF_5_unique.soapcov
```
# Pipeline used to obtain male and female genomic coverage in the asexuals
## Read mapping

First we indexed the chromosome level assembly genome of A. sinica

```
module load bowtie2
module load soap/coverage
#run commands on SLURM's srun
srun bowtie2-build /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq KazakhstanGenome_Scaffold
```
Then we mapped the reads to the indexed reference genome
```
srun bowtie2 -x KazakhstanGenome_Scaffold -1 40638AF_8_1.fastq -2 40638AF_8_2.fastq --end-to-end --sensitive -p 8 -S 40638AF_8.sam
srun bowtie2 -x KazakhstanGenome_Scaffold -1 40639AF_8_1.fastq -2 40639AF_8_2.fastq --end-to-end --sensitive -p 8 -S 40639AF_8.sam
srun bowtie2 -x KazakhstanGenome_Scaffold -1 99732RM_6_1.fastq -2 99732RM_6_2.fastq --end-to-end --sensitive -p 8 -S 99732RM_6.sam
srun bowtie2 -x KazakhstanGenome_Scaffold -1 99733AF_6_1.fastq -2 99733AF_6_2.fastq --end-to-end --sensitive -p 8 -S 99733AF_6.sam
```

We then extracted unique reads from the mapped libraries

```

srun grep -vw "XS:i" 40638AF_8.sam > 40638AF_8_unique.sam
srun grep -vw "XS:i" 40639AF_8.sam > 40639AF_8_unique.sam
srun grep -vw "XS:i" 99732RM_6.sam > 99732RM_6_unique.sam
srun grep -vw "XS:i" 99733AF_6.sam > 99733AF_6_unique.sam
```
## Coverage estimation

We computed the DNA coverage for each mapped library using soapcoverage. 

```

srun soap.coverage -sam -cvg -i 40638AF_8_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 40638AF_8_unique.soapcov

srun soap.coverage -sam -cvg -i 40639AF_8_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 40639AF_8_unique.soapcov

srun soap.coverage -sam -cvg -i 99732RM_6_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 99732RM_6_unique.soapcov

srun soap.coverage -sam -cvg -i 99733AF_6_unique.sam -onlyuniq -p 8 -refsingle /nfs/scistore03/vicosgrp/vbett/Artemia_NewKazgenome/k41_scaffolds.scafSeq -o 99733AF_6_unique.soapcov
```
# Pipeline used to obtain male and female genomic coverage in A. urmiana (sexual):

## Read mapping
First we indexed the chromosome level assembly genome of A. sinica

```
module load bowtie2/2.4.4
module load soap/coverage

srun bowtie2-build k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq KazakhstanGenomeUrm_Scaffold
```
Then we mapped the reads to the indexed reference genome
```
srun bowtie2 -x KazakhstanGenomeUrm_Scaffold -1 168696_S1_L001_R1_001.fastq.gz -2 168696_S1_L001_R2_001.fastq.gz --end-to-end --sensitive -p 8 -S 168696UM_1.sam
srun bowtie2 -x KazakhstanGenomeUrm_Scaffold -1 168697_S2_L001_R1_001.fastq.gz -2 168697_S2_L001_R2_001.fastq.gz --end-to-end --sensitive -p 8 -S 168697UM_2.sam
srun bowtie2 -x KazakhstanGenomeUrm_Scaffold -1 168698_S3_L001_R1_001.fastq.gz -2 168698_S3_L001_R2_001.fastq.gz --end-to-end --sensitive -p 8 -S 168698UF_1.sam
srun bowtie2 -x KazakhstanGenomeUrm_Scaffold -1 168699_S4_L001_R1_001.fastq.gz -2 168699_S4_L001_R2_001.fastq.gz --end-to-end --sensitive -p 8 -S 168699UF_2.sam
```

We then extracted unique reads from the mapped libraries

```

srun grep -vw "XS:i" 168696UM_1.sam > 168696UM_1_unique.sam
srun grep -vw "XS:i" 168697UM_2.sam > 168697UM_2_unique.sam
srun grep -vw "XS:i" 168698UF_1.sam > 168698UF_1_unique.sam
srun grep -vw "XS:i" 168699UF_2.sam > 168699UF_2_unique.sam
```
## Coverage estimation

We computed the DNA coverage for each mapped library using soapcoverage. 

```
srun soap.coverage -sam -cvg -i 168696UM_1_unique.sam -onlyuniq -p 8 -refsingle k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq -o 168696UM_1_unique.soapcov

srun soap.coverage -sam -cvg -i 168697UM_2_unique.sam -onlyuniq -p 8 -refsingle k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq -o 168697UM_2_unique.soapcov

srun soap.coverage -sam -cvg -i 168698UF_1_unique.sam -onlyuniq -p 8 -refsingle k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq -o 168698UF_1_unique.soapcov

srun soap.coverage -sam -cvg -i 168699UF_2_unique.sam -onlyuniq -p 8 -refsingle k41_scaffolds_kazakh_male_genome_16_09_2021.scafSeq -o 168699UF_2_unique.soapcov
```
