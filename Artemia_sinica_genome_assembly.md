# Artemia Sinica Chromosome Level Assembly

This is a description of the steps I followed to produce the Artemia sinica male genome assembly. The reads can be found in the follwing locations of the cluster:

Male short reads:

Male pacbio reads:
```
/archive3/group/vicosgrp/shared/ArtemiaProject/Genomes/Flye_assemblies/Asinica_Male_pacbio_genome/Mpacbio.fasta
```
Hi-C data:
```
/archive3/group/vicosgrp/shared/Artemia_HiC/HVC3MDSXY/135613/
```
## Flye:
For the long read male assembly, I used Flye with the following commands:
```
module load python/3

python /nfs/scistore03/vicosgrp/melkrewi/Flye/Flye/bin/flye --pacbio-raw Mpacbio.fasta --genome-size 1g --threads 20 --out-dir ./assembly
```
The resulting assembly was labeled assembly.fasta and can be found in:
```
/archive3/group/vicosgrp/shared/ArtemiaProject/Genomes/Flye_assemblies/Asinica_Male_pacbio_genome/
```
In order to check the contiguity of the assembly I used:
```
module load assembly-stats
assembly-stats assembly.fasta
```
The results for the male assembly:
- sum = 1812384600, n = 13618, ave = 133087.43, largest = 9925925
- N50 = 793586, n = 540
- N60 = 565033, n = 812
- N70 = 370053, n = 1208
- N80 = 203117, n = 1863
- N90 = 86188, n = 3226
- N100 = 330, n = 13618
- N_count = 16100
- Gaps = 161

In order to polish the male assembly using Pilon I followed the steps described by Beatriz below:
```
###pilon error correction
Folder: /nfs/scistore03/vicosgrp/bvicoso/Asinica_assembly_April2019/8-CanuMpbAssembly/Pilon
#Get draft genome and male reads (iso-female to keep things as clean as possible):
ln -s ../../1-Reads/Asin_IsoF_M_*paired.fastq .

#Align reads (Bowtie.sh and Bowtie2.sh)
module load bowtie2/2.2.9
srun bowtie2-build assembly.fasta Asin
module load bowtie2/2.2.9
srun bowtie2 -p 30 -x Asin -1 Asin_IsoF_M_1_paired.fastq -2 Asin_IsoF_M_2_paired.fastq -S AsinIsoF_M.sam

#Make sorted BAM file and indexes (SortBAM.sh)
module load samtools/1.8
srun samtools view -bS AsinIsoF_M.sam | samtools sort /dev/stdin -o AsinIsoF_M.sorted.bam
srun samtools index AsinIsoF_M.sorted.bam
srun samtools faidx assembly.fasta

#Run Pilon (Pilon.sh)
srun java -Xmx350G -jar /nfs/scistore03/vicosgrp/bvicoso/scripts/pilon-1.23.jar --genome assembly.fasta --frags AsinIsoF_M.sorted.bam --output AsinicaMpbPilon_flye --fix all --changes --verbose --threads 32 --diploid
```
The resulting assembly "AsinicaMpbPilon_flye.fasta" and the scripts I used can be found in:
```
/archive3/group/vicosgrp/shared/ArtemiaProject/Genomes/Flye_assemblies/Asinica_Male_pacbio_genome/Pilon/
```
The assembly stats:
- sum = 1809023064, n = 13618, ave = 132840.58, largest = 9906702
- N50 = 791316, n = 540
- N60 = 562680, n = 812
- N70 = 370135, n = 1207
- N80 = 202877, n = 1862
- N90 = 86138, n = 3226
- N100 = 330, n = 13618
- N_count = 14543
- Gaps = 160

## Miniasm:
I used minisiam and racon with the following commands:

```
/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/minimap/minimap2/minimap2 -x ava-pb -t 23 \
Mpacbio.fasta \
Mpacbio.fasta \
> minimap2_pacbio_oly.paf

gzip -1 minimap2_pacbio_oly.paf

/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/miniasm/miniasm/miniasm -f Mpacbio.fasta minimap2_pacbio_oly.paf.gz > pacbio_miniasm_reads.gfa

/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/minimap/minimap2/minimap2 -t 23 pacbio_miniasm_reads.fasta minimap2_pacbio_oly.paf.gz > minimap2_mapping_fasta_pacbio.paf

/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/minimap/minimap2/minimap2 -t 23 pacbio_miniasm_reads.fasta Mpacbio.fasta | gzip -1 > minimap2_mapping_fasta_pacbio.paf

/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/miniasm/racon/build/bin/racon Mpacbio.fasta minimap2_mapping_fasta_pacbio.paf pacbio_miniasm_reads.fasta
```
Then I polished the assembly using short reads as follows:
```
module load bwa

export PATH=/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/minimap/minimap2/minimap2:$PATH

bwa index pacbio_miniasm_racon.fasta
bwa mem -t 30 pacbio_miniasm_racon.fasta Asin_IsoF_M_1_paired.fastq.gz Asin_IsoF_M_2_paired.fastq.gz | samtools sort -O SAM | /nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/WTDBG/wtdbg2/wtpoa-cns -t 30 -x sam-sr -d pacbio_miniasm_racon.fasta -i - -fo pacbio_miniasm_racon_srp.fasta
```
The assembly stats:
- sum = 1798524669, n = 5310, ave = 338705.21, largest = 4244182
- N50 = 570290, n = 819
- N60 = 435233, n = 1181
- N70 = 320035, n = 1662
- N80 = 227950, n = 2325
- N90 = 141470, n = 3321
- N100 = 2173, n = 5310
- N_count = 0
- Gaps = 0


## Quickmerge:

I merged the Flye and Miniasm assemblies using Quickmerge:
```
module load python

export PATH=/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/quickmerge/quickmerge-0.3:/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/quickmerge/quickmerge-0.3/MUMmer3.23:$PATH

python /nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/quickmerge/quickmerge-0.3/merge_wrapper.py pacbio_miniasm_racon_srp.fasta AsinicaMpbPilon_flye.fasta

```
The assembly stats:
- sum = 1958395320, n = 2268, ave = 863490.00, largest = 18377270
- N50 = 1780764, n = 268
- N60 = 1315648, n = 398
- N70 = 945497, n = 574
- N80 = 605541, n = 833
- N90 = 345119, n = 1260
- N100 = 16130, n = 2268
- N_count = 7740
- Gaps = 86

## Purging the male assembly
I used purge_dups to remove duplicates and purge haplotigs:

```
module load python/3.7

export PATH=/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/minimap/minimap2/minimap2:$PATH

/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/minimap/minimap2/minimap2 -t 40 -xmap-pb merged_out.fasta Mpacbio.fasta | gzip -c - > merged_out.paf.gz
/nfs/scistore03/vicosgrp/melkrewi/Project_confirm_genome_assembly/purge/purge_dups/bin/pbcstat *.paf.gz
/nfs/scistore03/vicosgrp/melkrewi/Project_confirm_genome_assembly/purge/purge_dups/bin/calcuts PB.stat > cutoffs 2>calcults.log

/nfs/scistore03/vicosgrp/melkrewi/Project_confirm_genome_assembly/purge/purge_dups/bin/split_fa merged_out.fasta  > merged_out.fasta.split
/nfs/scistore03/vicosgrp/melkrewi/Improved_genome_project/minimap/minimap2/minimap2 -t 40 -xasm5 -DP merged_out.fasta.split merged_out.fasta.split | gzip -c - > merged_out.fasta.split.self.paf.gz

/nfs/scistore03/vicosgrp/melkrewi/Project_confirm_genome_assembly/purge/purge_dups/bin/purge_dups -2 -T cutoffs -c PB.base.cov merged_out.fasta.split.self.paf.gz > dups.bed 2> purge_dups.log

/nfs/scistore03/vicosgrp/melkrewi/Project_confirm_genome_assembly/purge/purge_dups/bin/get_seqs -e dups.bed merged_out.fasta

```
The purged stats:
- sum = 1916402944, n = 2256, ave = 849469.39, largest = 18377270
- N50 = 1733731, n = 262
- N60 = 1302888, n = 390
- N70 = 932288, n = 564
- N80 = 588226, n = 823
- N90 = 335508, n = 1252
- N100 = 16130, n = 2256
- N_count = 7540
- Gaps = 84


## Scaffolding the Male assembly:

I first removed PCR duplicated from the Hi-C data using Clumpify (BBmap):
```
clumpify.sh in=135613_S1_L003_R1_001.fastq.gz in2=135613_S1_L003_R2_001.fastq.gz out1=clumped_1.fastq.gz out2=clumped_2.fastq.gz dedupe subs=0
```

Then I mapped the reads to the purged assembly with the Arima pipeline:
```
module load bwa

module load samtools/1.13

bwa index purged.fa
bwa faidx purged.fa

echo "### Step 1.A: FASTQ to BAM (1st)"
bwa mem -t 60 purged.fa clumped_1.fastq.gz | samtools view -@ 25 -Sb - > mate_R1.bam

echo "### Step 1.B: FASTQ to BAM (2nd)"
bwa mem -t 60 purged.fa clumped_2.fastq.gz | samtools view -@ 40 -Sb - > mate_R2.bam

samtools view -@ 25 -h mate_R1.bam | perl /nfs/scistore03/vicosgrp/melkrewi/Arima/mapping_pipeline/filter_five_end.pl | samtools view -@ 25 -Sb - > mate_R1_f.bam

samtools view -@ 25 -h mate_R2.bam | perl /nfs/scistore03/vicosgrp/melkrewi/Arima/mapping_pipeline/filter_five_end.pl | samtools view -@ 25 -Sb - > mate_R2_f.bam

echo "### Step 3A: Pair reads & mapping quality filter"
perl /nfs/scistore03/vicosgrp/melkrewi/Arima/mapping_pipeline/two_read_bam_combiner.pl mate_R1_f.bam mate_R2_f.bam samtools 5 | samtools view -@ 30 -bS -t purged.fasta.fai - | samtools sort -@ 30 -n -o mate_merged_mapq5.bam -
```

Then I used YaHS 1.1 with the default settings to scaffold the purged assembly:

```
/nfs/scistore03/vicosgrp/melkrewi/genome_assembly_december_2021/21.yahs_lower_mapq/yahs_1.1/yahs-1.1a/yahs purged.fa mate_merged_mapq5.bam
```
The assembly stats:

To visualize the Hi-C contacts using juicer:
```
/nfs/scistore03/vicosgrp/melkrewi/genome_assembly_december_2021/26.plot_mapq5/plot_for_editing/yahs/juicer_pre -a -o test yahs.out.bin yahs.out_scaffolds_final.agp purged.fa.fai

java -jar -Xmx300G /nfs/scistore03/vicosgrp/melkrewi/genome_assembly_december_2021/26.plot_mapq5/plot_for_editing/juicer_tools.1.9.9_jcuda.0.8.jar pre test.txt test.hic <(echo "assembly 1701180553")
```

Contact Map:
<img src="contact_map.jpg">







